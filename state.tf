terraform {
  backend "gcs" {
    bucket  = "equifax-demo-tfstate"
    prefix  = "terraform/state/import-resources-gcp"
  }
}
