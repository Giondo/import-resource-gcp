provider "google" {
  # credentials = "${file("account.json")}"
  project     = "{var.PROJECT}"
  region      = "{var.REGION}"
  zone        = "{var.ZONE}"
}
