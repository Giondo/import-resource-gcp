variable "REGION" {
	default = "us-central1"
}
variable "ZONE" {
	default = "us-central1-a"
}
variable "PROJECT" {
	default = "equifax-demo-258920"
}
