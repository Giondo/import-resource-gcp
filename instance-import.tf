resource "google_compute_instance" "importvm" {
  name         = "instance-import"
  machine_type = "n1-standard-1"
  zone         = "us-central1-a"

  boot_disk {
      auto_delete = true
      device_name = "instance-import"
      mode        = "READ_WRITE"
      source      = "https://www.googleapis.com/compute/v1/projects/equifax-demo-258920/zones/us-central1-a/disks/instance-import"

      initialize_params {
          image  = "https://www.googleapis.com/compute/v1/projects/debian-cloud/global/images/debian-9-stretch-v20191121"
          labels = {}
          size   = 10
          type   = "pd-standard"
      }
  }
  scheduling {
      automatic_restart   = true
      on_host_maintenance = "MIGRATE"
      preemptible         = false
  }


  network_interface {
    network = "default"

    access_config {
      network_tier = "PREMIUM"
      // Ephemeral IP
    }
  }

  service_account {
      email  = "443521379678-compute@developer.gserviceaccount.com"
      scopes = [
          "https://www.googleapis.com/auth/devstorage.read_only",
          "https://www.googleapis.com/auth/logging.write",
          "https://www.googleapis.com/auth/monitoring.write",
          "https://www.googleapis.com/auth/service.management.readonly",
          "https://www.googleapis.com/auth/servicecontrol",
          "https://www.googleapis.com/auth/trace.append",
      ] 
  }

 
}